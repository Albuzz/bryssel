var fse = require('fs-extra');
var colors = require('colors');

fse.copy('src/styles', 'dist/styles', function (err) {
  if (err) return console.log(err.bold.red);
  console.log('/styles is copied to /dist'.green);
});

fse.copy('src/img', 'dist/img', function (err) {
  if (err) return console.log(err.bold.red);
  console.log('/img is copied to /dist'.green);
});

fse.copy('src/noreactjs', 'dist/noreactjs', function (err) {
  if (err) return console.log(err.bold.red);
  console.log('/noreactjs is copied to /dist'.green);
});
