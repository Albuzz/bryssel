// This script copies src/index.html into /dist/index.html
// and adds TrackJS error tracking code for use in production
// when useTrackJs is set to true below and a trackJsToken is provided.
// This is a good example of using Node and cheerio to do a simple file transformation.
// In this case, the transformation is useful since we only want to track errors in the built production code.
var fs = require('fs');
var colors = require('colors');
var cheerio = require('cheerio');

process.env.NODE_ENV='production';

var React = require('react');
var ReactDOMServer = require('react-dom/server');
var App = require('../src/containers/App');


var Webpack_isomorphic_tools = require('webpack-isomorphic-tools');

// this must be equal to your Webpack configuration "context" parameter
var project_base_path = require('path').resolve(__dirname, '..');

// this global variable will be used later in express middleware
global.webpack_isomorphic_tools = new Webpack_isomorphic_tools(require('../webpack-isomorphic-tools-configuration'))

  .server(project_base_path, function()
  {
    fs.readFile('src/index.html', 'utf8', function (err,data) {
      if (err) {
        return console.log(err.bold.red);
      }

      var $ = cheerio.load(data);
      $('title').text('SVT Nyheter – Explosionerna i Bryssel');
      $('#og_title').attr('content', 'SVT Nyheter – Tidslinje över explosionerna i Bryssel');
      $('#og_url').attr('content', 'http://pejl.svt.se/explosionerna-i-bryssel/');
      $('#og_description').attr('content', 'Två explosioner inträffade på tisdagsmorgonen på Bryssels flygplats Zavantem. En dryg timme senare skedde en explosion vid tunnelbanestationen Maelbaek.');
      $('#og_image').attr('content', 'http://pejl.svt.se/explosionerna-i-bryssel/img/flygplats-big.png');

      $('head').append('<link rel="stylesheet" href="bundle/style.css?2">'); //add built style

      $('#app').append(ReactDOMServer.renderToString(<App/>));

      if(!true) {    // use jQuery instead of react
        $('#scriptbundle').remove();
        $('body').append('<script src="noreactjs/noreact.js"></script>'); //add built style
      } else {
        $('#scriptbundle').attr("src", function(index, src) {
          return src + "?" + Date.now(); //Cachebusting on svtstatic
        })
      }

      fs.writeFile('dist/index.html', $.html(), 'utf8', function (err) {
        if (err) return console.log(err.red);
      });

      console.log('index.html written to /dist'.green);
    });
  });

