import React from 'react';
import Timeline from '../components/Timeline/Timeline';
import TimelineEvent from '../components/Timeline/TimelineEvent';
import NewsBlock from '../components/NewsBlock/NewsBlock';
import Text from '../components/NewsBlock/Text';
import Image from '../components/NewsBlock/Image';
import SimpleTable from '../components/NewsBlock/SimpleTable';
import Video from '../components/NewsBlock/Video';
import Map from '../components/Map/Map';
import Bylines from '../components/Bylines/Bylines';
import ShareButtons from '../components/Sharebuttons/Sharebuttons';

const App = React.createClass({
  displayName: "App",
  render () {
    const css = require('./App.scss');
    return (
      <div style={{position: 'relative', fontFamily: 'Publik', fontSize: '16px'}}>
        <h1 className={css.header}>Tidslinje: Explosionerna i Bryssel</h1>
        <p className={css.lead}>Belgien skakades den 22 mars 2016 av flera terrordåd. Två bomber briserade inne i huvudstadens flygplats. En timme senare inträffade en ny explosion vid en tunnelbanestation i centrum. 35 människor, inklusive tre självmordsbombare, dödades. Bland de döda fanns också två svenska kvinnor. Över 300 personer skadades.</p>
        <Timeline>
          <TimelineEvent time="Kl. 08">
            <NewsBlock title="Två explosioner vid Bryssels flygplats">
              <Text>
                <Map />
              </Text>
              <Text>
                <p>Vid 8-tiden den 22 mars 2016, inträffar två explosioner i avgångshallen på Bryssels flygplats. Flygplatsen stängs ner och evakueras.</p>
              </Text>
            </NewsBlock>

            <NewsBlock title="Video: Människor flyr i panik från flygplatsen">
              <Video id="7325695"/>
              <Text>
                <p>En privatperson fångar flyende resenärer på bild.</p>
              </Text>
            </NewsBlock>

            <NewsBlock title="Förödelse vid incheckningen">
              <Image src="img/terminal.png" />
              <Text>
                <p>På bilder från sociala medier syns förödelsen vid incheckningsdisk 3, där minst en av explosionerna tros ha ägt rum. Foto: Bryssels flygplats, @fduboccage.</p>
              </Text>
            </NewsBlock>

            <NewsBlock title={'Video: Skräcken i terminalen efter attacken'}>
              <Video id="7333693"/>
              <Text>
                <p>Folk försöker ta sig från den rökfyllda och kaotiska platsen.</p>
              </Text>
            </NewsBlock>

            <NewsBlock title={'Video: "Det kändes som en våg"'}>
              <Video id="7327543"/>
              <Text>
                <p>Ögonvittnen berättar att explosionen kändes i kroppen, och att de bestämde sig för att fly från platsen.</p>
              </Text>
            </NewsBlock>
          </TimelineEvent>

          <TimelineEvent time="Kl. 09.15">
            <NewsBlock title="Ytterligare en explosion – på tunnelbanan">
            <Image src="img/tunnelbana.jpg" />
            <Text>
              <p>Klockan 09.15 sker en explosion i ett tåg vid tunnelbanestationen Maelbeek i centrala Bryssel. Tunnelbanan var på väg västerut mot knutpunkten Arts-Loi.</p>
            </Text>
          </NewsBlock>

            <NewsBlock title="Video: Skadade vårdas direkt på marken">
              <Video id="7329535"/>
              <Text>
                <p>Privat video</p>
              </Text>
            </NewsBlock>

            <NewsBlock title="Video: Människor letar sig ut ur tunnlarna">
              <Video id="7328959"/>
              <Text>
                <p>Passagerare i tunnelbanan bryter själva upp dörrarna och går ner på spåren och sig till stationen Maelbeek.</p>
              </Text>
            </NewsBlock>
          </TimelineEvent>

          <TimelineEvent time="Kl. 11.00">
            <NewsBlock title="Belgien bekräftar terrordåd">
              <Text>
                <p>Den belgiska regeringen bekräftar att det rör sig om ett terrordåd och säger att minst en självmordsbombare deltagit i dådet.</p>
              </Text>
              <Text>
                <iframe src="//datawrapper.dwcdn.net/cAvYK/1/" frameborder="0" allowtransparency="true" allowfullscreen="allowfullscreen" webkitallowfullscreen="webkitallowfullscreen" mozallowfullscreen="mozallowfullscreen" oallowfullscreen="oallowfullscreen" msallowfullscreen="msallowfullscreen" width="100%" height="619"></iframe>
              </Text>
            </NewsBlock>
          </TimelineEvent>

          <TimelineEvent time="Kl. 16">
            <NewsBlock title="IS tar på sig dåden">
              <Image src="http://www.svtstatic.se/cms-image/cachable_image/1458660897/svts/article7337617.svt/alternates/large_cinema/media-xll-8510203-jpg" />
              <Text>
                <p style={{marginBottom: '1rem'}}>Nyhetsbyrån Reuters rapporterar att terrorgruppen Islamiska staten tar på sig attentaten. Samtidigt har en bild publicerats på tre män som misstänks för dåden på flygplatsen. Den federale åklagaren Frédéric Leeuw uppger att två av dem är döda (till vänster på bilden). Belgisk polis gick under kvällen ut med en efterlysning på den tredje mannen (till höger). Mannen i hatten till höger identifierades sedan som Mohamed Abrini och greps drygt två veckor efter attentaten.</p>
                <p><b>Läs SVT Nyheters <a href="http://www.svt.se/nyheter/live/live-explosioner-pa-bryssels-flygplats">direktrapportering från dagen för terrorattentaten</a></b></p>
              </Text>
            </NewsBlock>
          </TimelineEvent>
        </Timeline>
        <Bylines />
        <ShareButtons title="Explosionerna i Bryssel" />
      </div>
    );
  }
});

export default App;
