"use strict";

import React, { PropTypes } from "react";

const ShareButtons = React.createClass({
  displayName: "Share",

  propTypes: {
    facebook: PropTypes.string,
    showPrint: PropTypes.bool,
    title: PropTypes.string.isRequired,
    twitter: PropTypes.string,
    url: PropTypes.string
  },

  getDefaultProps() {
    return {
      facebook: "Dela",
      showPrint: true,
      twitter: "Dela",
      url: typeof window !== 'undefined' && window.location.href
    };
  },
  _print: function(e) {
    this._trackEvent("Print");
    e.preventDefault();
    window.print();
  },

  _openPopup: function(args, e) {
    var url = args.url;
    var propArr = [];
    var name = "win_" + Math.floor(Math.random() * 1000 + 1);
    var win;

    propArr.push("status=" + 0);
    propArr.push("toolbar=" + 0);
    propArr.push("location=" + 0);
    propArr.push("menubar=" + 0);
    propArr.push("directories=" + 0);
    propArr.push("resizable=" + 1);
    propArr.push("scrollbars=" + 0);
    propArr.push("height=" + args.height || 480);
    propArr.push("width=" + args.width || 640);
    win = window.open(url, name, propArr.join(","));

    if (win) {
      e.preventDefault();
      win = undefined;
    }
  },

  render: function() {
    const css = require("./ShareButtons.scss");
    var fullUrl = encodeURI(this.props.url);

    var emailBody = encodeURI("Jag vill tipsa om den här artikeln på svt.se/nyheter") + "%0D%0A" + encodeURI(`"${this.props.title}"`) + "%0D%0A" + encodeURI(fullUrl);
    var emailSubject = encodeURI(`SVT Nyheter: ${this.props.title}`);
    var emailUrl = `mailto:?subject=${emailSubject}&body=${emailBody}`;
    var facebookUrl = encodeURI(`https://www.facebook.com/sharer/sharer.php?u=${fullUrl}`);
    var twitterUrl = encodeURI(`https://twitter.com/intent/tweet?via=svtnyheter&related=svtnyheter&lang=sv&text=${this.props.title}&url=${fullUrl}`);

    var PotentialPrint = this.props.showPrint &&
      (
        <a href="javascript:window.print()" onClick={this._print} className="nyh_share__button nyh_share__button--print">
          <span className="svt_icon svt_icon--printer"/>
          <span className="nyh_share__text">Skriv ut</span>
        </a>
      );

    return (
        <section className="nyh_share nyh_share--article">
            <a onClick={e => this._openPopup({url: facebookUrl, width: 500, height: 250, sharedTo: "Facebook"}, e)}
               href={facebookUrl}
               target="_share"
               className="nyh_share__button nyh_share__button--facebook">
              <span className="svt_icon svt_icon--facebook-square"/>
              <span className="nyh_share__text">{this.props.facebook || "Dela"}</span>
            </a>
            <a onClick={e => this._openPopup({url: twitterUrl, width: 550, height: 520, sharedTo: "Twitter"}, e)}
               href={twitterUrl}
               target="_share"
               className="nyh_share__button nyh_share__button--twitter">
              <span className="svt_icon svt_icon--twitter"/>
              <span className="nyh_share__text">{this.props.twitter || "Dela"}</span>
            </a>
            <a href={emailUrl}
                 className="nyh_share__button nyh_share__button--mail">
                <span className="svt_icon svt_icon--envelope"/>
                <span className="nyh_share__text">E-post</span>
            </a>
            {PotentialPrint}
        </section>
    );
  }
});

export default ShareButtons;
