import React from "react";

const Map = props => {
  const styles = require("./Map.scss");
  return (
    <div id="g-flygplats-box" className="ai2html">
      <div id="g-flygplats-big" className="g-artboard " data-min-width="500">
        <div id="g-flygplats-big-graphic">
          <img id="g-ai0-0"
            className="g-aiImg"
            src="img/flygplats-big.png"
            />
          <div id="g-ai0-1" className="g-Layer_1 g-aiAbs" style={{top: "21.7949%", left: "69.6374%", width: "9.2388%"}}>
            <p className="g-aiPstyle0">Gater</p>
          </div>
          <div id="g-ai0-2" className="g-Layer_1 g-aiAbs" style={{top: "48.6264%", left: "46.9888%"}}>
            <p className="g-aiPstyle1">X</p>
          </div>
          <div id="g-ai0-3" className="g-Layer_1 g-aiAbs" style={{top: "52.0147%", left: "56.7806%", width: "12.8306%"}}>
            <p className="g-aiPstyle2">Avgångshallen</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Map;
