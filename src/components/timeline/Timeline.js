import React from 'react';

const Timeline = props => {
  const { children } = props;
  const style = require('./Timeline.scss');
  return (
    <div className={style.wrapper}>
      {children}
    </div>
  );
};

export default Timeline;
