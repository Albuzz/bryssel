import React from 'react';

const TimelineEvent = props => {
  const { time, children } = props;
  const style = require('./TimelineEvent.scss');
  return (
    <div className={style.wrapper}>
      <h4 className={style.time}>{time}</h4>
      <div className={style.content}>
        {children}
      </div>
    </div>
  );
};

export default TimelineEvent;
