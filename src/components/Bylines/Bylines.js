'use strict';
import React, { PropTypes } from 'react';

const Bylines = React.createClass({
  displayName: "Bylines",

  render() {
    const css = require('./Bylines.scss');

    return (
      <div className={css.wrapper}>
        <p className={css.bylines}>
          Av: <a className={css.byline} href={"mailto:robin.linderborg@svt.se"}>{'Robin Linderborg'}</a>
          {', '}<a className={css.byline} href={"mailto:fredrik.edgren@svt.se"}>{'Fredrik Edgren'}</a>
          {' och '}<a className={css.byline} href={"mailto:linnea.heppling@svt.se"}>{'Linnea Heppling'}</a>
        </p>
      </div>
    );
  }
});

export default Bylines;
