import React from 'react';

const Text = props => {
  const { children } = props;
  const style = require('./Text.scss');
  return (
    <div className={style.wrapper}>
       {children}
    </div>
  );
};

export default Text;
