import React from 'react';

const Image = props => {
  const style = require('./Image.scss');
  return (
    <div className={style.wrapper}>
      <img {...props}/>
    </div>
  );
};

export default Image;
