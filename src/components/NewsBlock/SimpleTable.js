import React, { PropTypes } from 'react';

function createTableHeaderRow(name) {
  return <th key={name}>{name}</th>;
}

function createTableRow(values) {
  return (
    <tr key={values[0]}>
      {values.map(value => <td key={value}>{value}</td>)}
    </tr>
  );
}

const SimpleTable = React.createClass({
  propTypes: {
    title: PropTypes.string,
    headers: PropTypes.array,
    rows: PropTypes.array
  },

  render: function() {
    const css = require('./SimpleTable.scss');
    const { title, headers, rows } = this.props;

    return (
      <div className={css.wrapper}>
        <h3 className={css.title}>{title}</h3>
        <table className={css.tables}>
          <thead>
          <tr>
            {headers.map(createTableHeaderRow)}
          </tr>
          </thead>
          <tbody>
          {rows.map(createTableRow)}
          </tbody>
        </table>
      </div>
    );
  }
});

export default SimpleTable;
