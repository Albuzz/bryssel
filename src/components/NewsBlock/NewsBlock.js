import React from 'react';

const NewsBlock = props => {
  const { title, children } = props;
  const style = require('./NewsBlock.scss');
  return (
    <div className={style.wrapper}>
      <h3 className={style.title}>{title}</h3>
      <div className={style.content}>
        {children}
      </div>
    </div>
  );
};

export default NewsBlock;
