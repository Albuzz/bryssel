import React from 'react';

const Video = React.createClass({
  render() {
    const { id, poster } = this.props;
    const style = require('./Video.scss');
    return (
      <div className={style.wrapper}>
        <div  className={style.player}>
          <iframe className={style.iframe} src={'http://www.svt.se/videoplayer-embed/' + id} width="100%" height="100%" frameBorder="0" allowFullScreen="true"></iframe>
        </div>
      </div>
    );
  }
});

export default Video;
