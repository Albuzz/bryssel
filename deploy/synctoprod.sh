#!/usr/bin/env bash


read -p "Are you sure you want to sync from stage to prod? (Y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
  rsync -avz static@svt-stoprod-static01:pejl/stage/explosionerna-i-bryssel/ deploy/temp/
  rsync -avz deploy/temp/ static@svt-drsprod-static01:pejl/explosionerna-i-bryssel/
  rsync -avz deploy/temp/ static@svt-stoprod-static01:pejl/explosionerna-i-bryssel/
  rm -r deploy/temp
  echo "stage is synced to http://pejl.svt.se/explosionerna-i-bryssel/"
    # do dangerous stuff
fi
